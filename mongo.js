// Select a database
use <database name>

// when creating a new database via the command line, the use command can be entered with a name of a database that does not yet exist. Once a new record is inserted into the datbase, the database will be created.

// Database = filing cabinet
// Collection = drawer
// Document = folder inside the drawer
// Sub-documents (optional) = other files
// Field = file content

/*	e.g. document with no sub-documents

{
	name: "Leonell Cruz",
	age: 28,
	occupation: "Web Developer",
}

*/

/*	e.g. document with sub-documents

{
	name: "Leonell Cruz",
	age: 28,
	occupation: "Web Developer",
	address: {
		street: "l1 b19 street",
		city: "Antipolo",
		country: "Philippines"
	}
}

*/

db.user.insert({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "12345678",
		email: "leo@mail.com"
	},
	courses: ["CSS", "JavaScript", "Phyton"],
	department: "none"
})

// insert many

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "hawking",
		age: 76,
		
		contact: {
			phone: "123456789",
			email: "stephen@mail.com"
		},
		courses: ["Phyton", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "123456789",
			email: "Neil@mail.com"
		},
		courses: ["React", "Laravel", "SASS"],
		department: "none"
	}

])

